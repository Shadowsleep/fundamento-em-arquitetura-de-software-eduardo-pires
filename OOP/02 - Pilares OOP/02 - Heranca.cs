﻿using System;

namespace OOP
{
    /**
     * Herança se entende do conjunto do comportamentos e estados comuns a varias classes
     * conceito de algo é outro?
     * Se sim herança se nao outra coisa menos herança.
     */
    public class Funcionario : Pessoa
    {
        public DateTime DataAdmissao { get; set; }
        public string Registro { get; set; }
    }

    public class Processo
    {
        public void Execucao()
        {
            var funcionario = new Funcionario()
            {
                Nome = "João da Silva",
                DataNascimento = Convert.ToDateTime("1990/01/01"),
                DataAdmissao = DateTime.Now,
                Registro = "0123456",
            };

            funcionario.CalcularIdade();
        }
    }
}