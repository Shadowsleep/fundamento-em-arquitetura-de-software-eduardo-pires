﻿using System;

namespace OOP
{
    /**
     * Estado - atributos
     * Comportamento - metodos
     * 
     * entende se que o estado é os atributos que podem ser modificados
     * e o comportamento é qualquer metodo q podem modificar o estado dos atributos.
     */
    public class Pessoa
    {
        public string Nome { get; set; }
        public DateTime DataNascimento { get; set; }

        public int CalcularIdade()
        {
            var dataAtual = DateTime.Now;
            var idade = dataAtual.Year - DataNascimento.Year;

            if (dataAtual < DataNascimento.AddYears(idade)) idade--;

            return idade;
        }
    }
}