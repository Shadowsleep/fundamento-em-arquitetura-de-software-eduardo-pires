﻿
namespace OOP
{
    /**
     *  esconder o comportamento das classes mais importantes que vc nao queria expor exemplo o repositorio de conexao com o banco
     */
    public class AutomacaoCafe
    {
        public void ServirCafe()
        {
            var espresso = new CafeteiraEspressa();
            espresso.Ligar();
            espresso.PrepararCafe();
            espresso.Desligar();
        }
    }
}