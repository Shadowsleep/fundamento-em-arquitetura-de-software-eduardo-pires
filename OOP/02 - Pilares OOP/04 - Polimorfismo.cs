﻿namespace OOP
{
    // Poli-morfismo
    /**
     *  exemplo de implementaçao da classe abstrata 
     */
    public class CafeteiraEspressa : Eletrodomestico
    {
        public CafeteiraEspressa(string nome, int voltagem)
            : base(nome, voltagem) { }

        public CafeteiraEspressa()
            : base("Padrao", 220) {  }

        private static void AquecerAgua() { }

        private static void MoerGraos() { }

        public void PrepararCafe()
        {
            Testar();
            AquecerAgua();
            MoerGraos();
            // ETC...
        }
        /**
         * Sobrecarga do metodo virtual
         */
        public void Testar()
        {
            // teste de cafeteira
        }
         /**
         *  obrigatoridade
         */
        public override void Ligar()
        {
            // Verificar recipiente de agua
        }
        /**
         *  obrigatoridade
         */
        public override void Desligar()
        {
            // Resfriar aquecedor
        }
    }
}