﻿namespace OOP
{
    /**
     *  Classe abstrata abstrai um conceito igualmente a classe herdada diferenca que a mesma nao pode ser instanciada
     */
    public abstract class Eletrodomestico
    {
        private readonly string _nome;
        private readonly int _voltagem;
        protected Eletrodomestico(string nome, int voltagem)
        {
            _nome = nome;
            _voltagem = voltagem;
        }

        public abstract void Ligar();
        /**
         * Metodo abtrato nao tem implementação ele tem q ser obrigatoriamente implementedo na classe via override (sobreescrito)
         */
        public abstract void Desligar();
        /**
         * Metodo virtual  tem implementação e pode ser sobreescrito
         */
        public virtual void Testar()
        {
            // teste do equipamento
        }
    }
}