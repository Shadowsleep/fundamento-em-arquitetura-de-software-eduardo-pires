﻿using System.Collections.Generic;

namespace DesignPatterns.AbstractFactory
{
    public class ExecucaoAbstractFactory
    {
        public static void Executar()
        {
            var veiculosSocorro = new List<Veiculo>
            {
                VeiculoCreator.Criar("Celta", Porte.Pequeno),
                VeiculoCreator.Criar("Jetta", Porte.Medio),
                VeiculoCreator.Criar("BMW X6", Porte.Grande)
            };


            veiculosSocorro.ForEach(v => AutoSocorro.CriarAutoSocorro(v).RealizarAtendimento());
        }
    }
}

/*
 * O Abstract Method funciona como fabricas de fabricas ou seja vc precisa criar dois objetos que possuem varias instacias cada um dele eles se conversam
 * entao vc cria uma fabrica pra cada um deles pra criar de acordo com o necessario
 * Cria uma fabrica abstrata pra ter a abstracao delas.
 * e no cliente a partir da classe abstrata das classes instanciar as concretas.
 */
