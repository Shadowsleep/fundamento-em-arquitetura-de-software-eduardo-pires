﻿namespace SOLID.OCP.Violacao
{
    public class DebitoConta
    {
        /*
         * A ideia é fechar a classe para mudança a classe via classe abstrata ou extensios methods, para adicionar comportamentos.
         * com iss ela nao sera modificada.
         * A ideia é que se a classe esta ganhando funcionalidades parecidas, entao deve criar classes secundarias com essas funcionalidade
         */
        public void Debitar(decimal valor, string conta, TipoConta tipoConta)
        {
            if (tipoConta == TipoConta.Corrente)
            {
                // Debita Conta Corrente
            }

            if (tipoConta == TipoConta.Poupanca)
            {
                // Valida Aniversário da Conta
                // Debita Conta Poupança
            }
        }
    }
}