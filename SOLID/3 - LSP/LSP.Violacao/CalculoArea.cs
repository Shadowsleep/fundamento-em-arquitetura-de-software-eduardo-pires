﻿using System;

namespace SOLID.LSP.Violacao
{
    /*
     * A qualquer metodo sobrecarregado tem q ter o mesmo comportamento q o da classe Pai, ou seja,
     * vc tem q trocar o sub classe pela super classe e todo o comportamento da sub tem q ser feita na super.
     */
    public class CalculoArea
    {
        private static void ObterAreaRetangulo(Retangulo ret)
        {
            Console.Clear();
            Console.WriteLine("Calculo da área do Retangulo");
            Console.WriteLine();
            Console.WriteLine(ret.Altura + " * " + ret.Largura);
            Console.WriteLine();
            Console.WriteLine(ret.Area);
            Console.ReadKey();
        }

        public static void Calcular()
        {
            var quad = new Quadrado()
            {
                Altura = 10,
                Largura = 5
            };

            ObterAreaRetangulo(quad);
        }
    }
}