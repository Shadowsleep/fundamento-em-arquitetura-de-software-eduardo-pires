﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID.SRP.Solucao.Estudo
{
    class Cliente
    {
        public int ClienteId { get; set; }
        public string Nome { get; set; }
        public Email Email { get; set; }
        public Cpf CPF { get; set; }
        public DateTime DataCadastro { get; set; }

        public bool Validar()
        {
            return CPF.Validar() && Email.Validar();
        }
    }
}
