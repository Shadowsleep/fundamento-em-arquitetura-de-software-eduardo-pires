﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID.SRP.Solucao.Estudo
{
    class ClienteService
    {
        public string AdicionarCliente(Cliente cliente )
        {
            if (!cliente.Validar())
            {
                return "nao valido";
            }
            //essa parte seria interessante a DIP
            var service = new ClienteRepository();
            service.AdicionarCliente(cliente);
            EmailService.Enviar("mim", "voce", "SRP", "parabens salvou");
            return "termino do SRP";
        }
    }
}
